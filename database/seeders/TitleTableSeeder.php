<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class TitleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('titles')->delete();

        DB::table('titles')->insert(['id' => 1, 'name' => 'Technical']);
        DB::table('titles')->insert(['id' => 2, 'name' => 'Business Manager']);
    }
}
