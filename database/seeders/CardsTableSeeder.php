<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class CardsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cards')->delete();

        DB::table('cards')->insert(['id' => 1, 'user' => 1, 'card_name' => 'Total Votes by Products', 'order' => 0, 'card_width' => 1, 'card_types_id' => 1]);
        DB::table('cards')->insert(['id' => 2, 'user' => 1, 'card_name' => 'Active Users', 'order' => 1, 'card_width' => 2, 'card_types_id' => 2]);
        DB::table('cards')->insert(['id' => 3, 'user' => 1, 'card_name' => 'Project Albums', 'order' => 2, 'card_width' => 1, 'card_types_id' => 3]);
    }
}
