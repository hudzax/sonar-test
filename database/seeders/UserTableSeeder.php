<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        DB::table('users')->insert([
            'id' => 1,
            'name' => 'Hammaam',
            'titles_id' => 1,
            'status_id' => 1,
            'roles_id' => 1,
            'email' => 'hudzax@gmail.com',
            'password' => '$2y$10$XvwZZ0hH33gfRpiQKfH81.Nqyx7/ISQPC2fdCzh39huMsEbf/.DNy'
        ]);

        DB::table('users')->insert([
            'id' => 2,
            'name' => 'Vita',
            'titles_id' => 2,
            'status_id' => 1,
            'roles_id' => 2,
            'email' => 'vita@gmail.com',
            'password' => '$2y$10$A7i0vFhSlzQGIzi5KdccPe.5w1rFS.Bhb8ykvChVZDVztGGExsbVq'
        ]);
    }
}
