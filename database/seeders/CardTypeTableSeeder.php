<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class CardTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('card_types')->delete();

        DB::table('card_types')->insert(['id' => 1, 'name' => 'Chart', 'accent' => 'yellow']);
        DB::table('card_types')->insert(['id' => 2, 'name' => 'Table', 'accent' => 'red']);
        DB::table('card_types')->insert(['id' => 3, 'name' => 'Image', 'accent' => 'green']);
    }
}
