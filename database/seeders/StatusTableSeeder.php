<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->delete();

        DB::table('statuses')->insert(['id' => 1, 'name' => 'Active']);
        DB::table('statuses')->insert(['id' => 2, 'name' => 'Disabled']);
    }
}
