## Frontend Programmer Test for Sonar Platform ##

You'll find the list of used libraries and installation instructions below.

### Used frameworks/libraries/plugins ###

* Laravel 8.12
* Laravel UI
* Tailwind CSS/UI
* Jquery
* Axios
* ChartJS
* AlpineJS

### Installation ###

* `git clone git@bitbucket.org:hudzax/sonar-test.git example`
* `cd example`
* `composer install`
* Create a database and set *.env*
* `php artisan key:generate`
* `php artisan migrate --seed` to create and populate tables
* `php artisan serve` to start the app on http://localhost:8000/