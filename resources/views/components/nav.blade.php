<nav class="bg-gray-800" x-data="{mobileOpen:false}">
    <div class="max-w-9xl mx-auto px-4 sm:px-6 lg:px-8">
    <div class="flex items-center justify-between h-16">
        <div class="flex items-center">
        <div class="flex-shrink-0">
            <svg class="h-8 text-blue-500" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M14 10l-2 1m0 0l-2-1m2 1v2.5M20 7l-2 1m2-1l-2-1m2 1v2.5M14 4l-2-1-2 1M4 7l2-1M4 7l2 1M4 7v2.5M12 21l-2-1m2 1l2-1m-2 1v-2.5M6 18l-2-1v-2.5M18 18l2-1v-2.5" />
            </svg>
        </div>
        <div class="hidden md:block">
            <div class="ml-3 flex items-baseline space-x-2">
            <a href="#" class="text-white px-2 py-2 text-base font-medium">Dashboard</a>
            <a href="#" class="text-gray-300 hover:text-white px-2 py-2 text-base font-medium">Projects</a>
            <a href="#" class="text-gray-300 hover:text-white px-2 py-2 text-base font-medium">Activities</a>
            <a href="#" class="text-gray-300 hover:text-white px-2 py-2 text-base font-medium">Campaigns</a>
            <div class="relative" x-data="{open:false}">
                <button @click="open=true" type="button" class="group inline-flex items-center text-gray-300 hover:text-white px-2 py-2 text-base font-medium">
                <span>Reports</span>
                <svg class="ml-1 mt-1 h-5 w-5 text-gray-300 group-hover:text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                </svg>
                </button>
                <div x-show="open" x-transition:enter="transition ease-out duration-100" x-transition:enter-start="opacity-0 transform scale-90" x-transition:enter-end="opacity-100 transform scale-100" x-transition:leave="transition ease-in duration-100" x-transition:leave-start="opacity-100 transform scale-100" x-transition:leave-end="opacity-0 transform scale-90" @click.away="open=false" class="absolute z-10 -ml-4 mt-3 transform px-2 w-screen max-w-md sm:px-0 lg:ml-0 lg:left-1/2 lg:-translate-x-1/2">
                <div class="rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 overflow-hidden">
                    <div class="relative grid gap-6 bg-white px-5 py-6 sm:gap-8 sm:p-8">
                    <a href="#" class="-m-3 p-3 flex items-center rounded-lg hover:bg-gray-50">
                        <!-- Heroicon name: chart-bar -->
                        <svg class="flex-shrink-0 h-6 w-6 text-blue-500" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 19v-6a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2a2 2 0 002-2zm0 0V9a2 2 0 012-2h2a2 2 0 012 2v10m-6 0a2 2 0 002 2h2a2 2 0 002-2m0 0V5a2 2 0 012-2h2a2 2 0 012 2v14a2 2 0 01-2 2h-2a2 2 0 01-2-2z" />
                        </svg>
                        <div class="ml-4">
                        <p class="text-base font-medium text-gray-900">
                            Analytics
                        </p>
                        <p class="mt-1 text-sm text-gray-500">
                            Get a better understanding of where your traffic is coming from.
                        </p>
                        </div>
                    </a>

                    <a href="#" class="-m-3 p-3 flex items-center rounded-lg hover:bg-gray-50">
                        <!-- Heroicon name: cursor-click -->
                        <svg class="flex-shrink-0 h-6 w-6 text-blue-500" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 15l-2 5L9 9l11 4-5 2zm0 0l5 5M7.188 2.239l.777 2.897M5.136 7.965l-2.898-.777M13.95 4.05l-2.122 2.122m-5.657 5.656l-2.12 2.122" />
                        </svg>
                        <div class="ml-4">
                        <p class="text-base font-medium text-gray-900">
                            Engagement
                        </p>
                        <p class="mt-1 text-sm text-gray-500">
                            Speak directly to your customers in a more meaningful way.
                        </p>
                        </div>
                    </a>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>
        <div class="hidden md:block">
        <div class="ml-4 flex items-center md:ml-6">
            <!-- Profile dropdown -->
            <div class="ml-3 relative" x-data="{open:false}">
            <div>
                <button @click="open=true" class="max-w-xs bg-gray-800 rounded-full flex items-center text-sm focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white" id="user-menu" aria-haspopup="true">
                <span class="sr-only">Open user menu</span>
                <img class="h-8 w-8 rounded-full border-2 border-gray-300 hover:border-white" src="https://picsum.photos/seed/user{{ Auth::user()->name }}/128" alt="">
                </button>
            </div>
            <div x-show="open" class="fixed inset-0 overflow-hidden z-10">
                <div class="absolute inset-0 overflow-hidden">
                <!--
                    Background overlay, show/hide based on slide-over state.

                    Entering: "ease-in-out duration-500"
                    From: "opacity-0"
                    To: "opacity-100"
                    Leaving: "ease-in-out duration-500"
                    From: "opacity-100"
                    To: "opacity-0"
                -->
                <div x-show="open" x-transition:enter="ease-in-out duration-500"
    x-transition:enter-start="opacity-0"
    x-transition:enter-end="opacity-100"
    x-transition:leave="ease-in-out duration-500"
    x-transition:leave-start="opacity-100"
    x-transition:leave-end="opacity-0" class="absolute inset-0 bg-gray-900 bg-opacity-75 transition-opacity" aria-hidden="true"></div>
                <section class="absolute inset-y-0 right-0 pl-10 max-w-full flex" aria-labelledby="slide-over-heading">
                    <!--
                    Slide-over panel, show/hide based on slide-over state.

                    Entering: "transform transition ease-in-out duration-500 sm:duration-700"
                        From: "translate-x-full"
                        To: "translate-x-0"
                    Leaving: "transform transition ease-in-out duration-500 sm:duration-700"
                        From: "translate-x-0"
                        To: "translate-x-full"
                    -->
                    <div x-show="open" x-transition:enter="transform transition ease-in-out duration-500 sm:duration-700"
    x-transition:enter-start="translate-x-full"
    x-transition:enter-end="translate-x-0"
    x-transition:leave="transform transition ease-in-out duration-500 sm:duration-700"
    x-transition:leave-start="translate-x-0"
    x-transition:leave-end="translate-x-full" @click.away="open=false" class="relative w-screen max-w-xs">
                    <!--
                        Close button, show/hide based on slide-over state.

                        Entering: "ease-in-out duration-500"
                        From: "opacity-0"
                        To: "opacity-100"
                        Leaving: "ease-in-out duration-500"
                        From: "opacity-100"
                        To: "opacity-0"
                    -->
                    <div x-show="open" x-transition:enter="ease-in-out duration-500"
    x-transition:enter-start="opacity-0"
    x-transition:enter-end="opacity-100"
    x-transition:leave="ease-in-out duration-500"
    x-transition:leave-start="opacity-100"
    x-transition:leave-end="opacity-0" class="absolute top-0 left-0 -ml-8 pt-4 pr-2 flex sm:-ml-10 sm:pr-4">
                        <button @click="open=false" class="rounded-md text-gray-300 hover:text-white focus:outline-none focus:ring-2 focus:ring-white">
                        <span class="sr-only">Close panel</span>
                        <!-- Heroicon name: x -->
                        <svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                        </svg>
                        </button>
                    </div>
                    <div class="h-full flex flex-col py-6 bg-gray-800 shadow-xl overflow-hidden">
                        <div class="flex items-center px-6">
                        <div class="flex-shrink-0">
                            <img class="h-14 w-14 rounded-full border-2 border-gray-200" src="https://picsum.photos/seed/user{{ Auth::user()->name }}/128" alt="">
                        </div>
                        <div class="ml-5 space-y-1">
                            <div class="text-lg font-semibold leading-none text-gray-200">{{ Auth::user()->name }}</div>
                            <div class="text-base font-medium leading-none text-gray-300">{{ Auth::user()->email }}</div>
                        </div>
                        </div>
                        <div class="mt-6 relative flex-1 px-6">
                        <ul class="space-y-2 text-sm">
                            <li>
                                <a href="#" class="flex items-center space-x-3 text-gray-300 hover:text-white py-2 font-medium">
                                    <svg class="h-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z" />
                                    </svg>
                                    <span>My profile</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="flex items-center space-x-3 text-gray-300 hover:text-white py-2 font-medium">
                                    <svg class="h-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z" />
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
                                    </svg>
                                    <span>Settings</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('logout') }}" class="flex items-center space-x-3 text-gray-300 hover:text-white py-2 font-medium">
                                    <svg class="h-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M20.354 15.354A9 9 0 018.646 3.646 9.003 9.003 0 0012 21a9.003 9.003 0 008.354-5.646z" />
                                    </svg>
                                    <span>Logout</span>
                                </a>
                            </li>
                        </ul>
                        </div>
                    </div>
                    </div>
                </section>
                </div>
            </div>
            </div>
        </div>
        </div>
        <div class="-mr-2 flex md:hidden">
        <!-- Mobile menu button -->
        <button @click="mobileOpen=true" class="bg-gray-800 inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white">
            <span class="sr-only">Open main menu</span>
            <!--
            Heroicon name: menu

            Menu open: "hidden", Menu closed: "block"
            -->
            <svg class="block h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
            </svg>
            <!--
            Heroicon name: x

            Menu open: "block", Menu closed: "hidden"
            -->
            <svg class="hidden h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
            </svg>
        </button>
        </div>
    </div>
    </div>

    <!-- Mobile menu -->
    <div x-show="mobileOpen" x-transition:enter="transition ease-out duration-200"
    x-transition:enter-start="opacity-0 transform scale-90"
    x-transition:enter-end="opacity-100 transform scale-100"
    x-transition:leave="transition ease-in duration-200"
    x-transition:leave-start="opacity-100 transform scale-100"
    x-transition:leave-end="opacity-0 transform scale-90" @click.away="mobileOpen=false" class="md:hidden" role="menu">
    <div class="px-2 pt-2 pb-3 space-y-1 sm:px-3">
        <div class="px-3 mb-4">
        <nav class="grid gap-y-5">
            <a href="#" class="-m-3 p-3 flex items-center rounded-md hover:bg-gray-50">
            <!-- Heroicon name: chart-bar -->
            <svg class="flex-shrink-0 h-6 w-6 text-indigo-600" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 19v-6a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2a2 2 0 002-2zm0 0V9a2 2 0 012-2h2a2 2 0 012 2v10m-6 0a2 2 0 002 2h2a2 2 0 002-2m0 0V5a2 2 0 012-2h2a2 2 0 012 2v14a2 2 0 01-2 2h-2a2 2 0 01-2-2z" />
            </svg>
            <span class="ml-3 text-base font-medium text-gray-300 hover:text-white">
                Analytics
            </span>
            </a>

            <a href="#" class="-m-3 p-3 flex items-center rounded-md hover:bg-gray-50">
            <!-- Heroicon name: cursor-click -->
            <svg class="flex-shrink-0 h-6 w-6 text-indigo-600" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 15l-2 5L9 9l11 4-5 2zm0 0l5 5M7.188 2.239l.777 2.897M5.136 7.965l-2.898-.777M13.95 4.05l-2.122 2.122m-5.657 5.656l-2.12 2.122" />
            </svg>
            <span class="ml-3 text-base font-medium text-gray-300 hover:text-white">
                Engagement
            </span>
            </a>
        </nav>
        </div>
        <!-- Current: "bg-gray-900 text-white", Default: "text-gray-300 hover:bg-gray-700 hover:text-white" -->
        <a href="#" class="bg-gray-900 text-white block px-3 py-2 rounded-md text-base font-medium">Dashboard</a>
        <a href="#" class="text-gray-300 hover:bg-gray-700 hover:text-white block px-3 py-2 rounded-md text-base font-medium">Projects</a>
    </div>
    <div class="pt-4 pb-3 border-t border-gray-700">
        <div class="flex items-center px-5">
        <div class="flex-shrink-0">
            <img class="h-10 w-10 rounded-full border-2 border-gray-300 hover:border-white" src="https://picsum.photos/seed/user{{ Auth::user()->name }}/128" alt="">
        </div>
        <div class="ml-3">
            <div class="text-base font-medium leading-none text-white">{{ Auth::user()->name }}</div>
            <div class="text-sm font-medium leading-none text-gray-400">{{ Auth::user()->email }}</div>
        </div>
        </div>
        <div class="mt-3 px-2 space-y-1">
        <a href="#" class="block px-3 py-2 rounded-md text-base font-medium text-gray-300 hover:text-white">My Profile</a>
        <a href="#" class="block px-3 py-2 rounded-md text-base font-medium text-gray-300 hover:text-white">Settings</a>
        <a href="{{ route('logout') }}" class="block px-3 py-2 rounded-md text-base font-medium text-gray-300 hover:text-white">Logout</a>
        </div>
    </div>
    </div>
</nav>