<x-nav/>
<x-layout>
  <header class="bg-white shadow">
    <div class="max-w-9xl mx-auto py-6 px-4 sm:px-6 lg:px-8 space-y-1">
      <h1 class="text-3xl font-bold leading-tight text-gray-700">
        Dashboard
      </h1>
      <div class="text-sm text-gray-500">
        Sortable cards. Sorted cards are automatically saved.
      </div>
    </div>
  </header>
  <main>
    <div id="cards-container" class="max-w-9xl mx-auto py-8 px-8">
      <!-- Cards -->
      <div id="cards-sortable" class="grid gap-6 mb-8 sm:grid-col-1 md:grid-cols-2 xl:grid-cols-3 select-none">
      @foreach ($cards as $card)
        <div card-id="{{ $card->id }}" class="card col-span-{{ $card->card_width}}">
          <div class="p-5 bg-white rounded shadow-sm hover:shadow dark:bg-gray-800 cursor-pointer space-y-5 relative">
            <div class="flex items-center">
              <div class="saving h-3 w-3 absolute top-4 right-4 rounded-full bg-orange-300 opacity-0"></div>
              <div class="p-3 mr-4 text-{{ $card->card_types->accent }}-500 bg-{{ $card->card_types->accent }}-100 rounded-full">
                <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                  @switch($card->card_types->id)
                    @case(1)
                      <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M9 19v-6a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2a2 2 0 002-2zm0 0V9a2 2 0 012-2h2a2 2 0 012 2v10m-6 0a2 2 0 002 2h2a2 2 0 002-2m0 0V5a2 2 0 012-2h2a2 2 0 012 2v14a2 2 0 01-2 2h-2a2 2 0 01-2-2z" />
                      @break
                    
                    @case(2)
                      <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M3 10h18M3 14h18m-9-4v8m-7 0h14a2 2 0 002-2V8a2 2 0 00-2-2H5a2 2 0 00-2 2v8a2 2 0 002 2z" />
                      @break
                    
                    @case(3)
                      <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M4 16l4.586-4.586a2 2 0 012.828 0L16 16m-2-2l1.586-1.586a2 2 0 012.828 0L20 14m-6-6h.01M6 20h12a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v12a2 2 0 002 2z" />
                      @break
  
                    @default
                  @endswitch
                </svg>
              </div>
              <div class="space-y-1">
                <p class="text-sm font-medium text-gray-600 dark:text-gray-400">
                  {{ $card->card_types->name }}
                </p>
                <p class="text-md font-semibold text-gray-700 dark:text-gray-200">
                  {{ $card->card_name }}
                </p>
              </div>
            </div>
            <div>
              @switch($card->card_types->id)
                @case(1)
                  <canvas class="card-charts"></canvas>
                  @break
  
                @case(2)
                  <div class="card-tables">
                    <div class="shadow overflow-hidden border-b border-gray-200 rounded">
                      <table class="min-w-full divide-y divide-gray-200">
                        <thead class="bg-gray-50">
                          <tr>
                            <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                              Name
                            </th>
                            <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                              Title
                            </th>
                            <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                              Status
                            </th>
                            <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                              Role
                            </th>
                          </tr>
                        </thead>
                        <tbody class="bg-white divide-y divide-gray-200">
                          @foreach ($active_users as $user)
                              <tr>
                                <td class="px-6 py-4 whitespace-nowrap">
                                  <div class="flex items-center">
                                    <div class="flex-shrink-0 h-10 w-10">
                                      <img class="h-10 w-10 rounded-full" src="https://picsum.photos/seed/user{{ $user->name }}/128" alt="">
                                    </div>
                                    <div class="ml-4">
                                      <div class="text-sm font-medium text-gray-900">
                                        {{ $user->name }}
                                      </div>
                                      <div class="text-sm text-gray-500">
                                        {{ $user->email }}
                                      </div>
                                    </div>
                                  </div>
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap">
                                  <div class="text-sm text-gray-900">{{ $user->titles->name }}</div>
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap">
                                  <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                                    {{ $user->status->name }}
                                  </span>
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                  {{ $user->roles->name }}
                                </td>
                              </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                  @break
  
                @case(3)
                  <img class="w-full rounded" src="https://picsum.photos/1280/720" alt="example photo">
                  @break
  
                @default
              @endswitch
            </div>
          </div>
        </div>
      @endforeach
      </div>
      <script>
        $(function() {
          // Init sortable cards
          $("#cards-sortable").sortable({
            axis:false,
            containment:"#cards-container",
            revert:true,
          });

          // autosave sorted cards
          $("#cards-sortable").on("sortupdate", function (event, ui) {
            let saving = $(".card[card-id=" + ui.item.attr('card-id') + "] .saving");
            saving.css("opacity", "100");
            let sorted = $(this).sortable("toArray", {attribute:"card-id"});
            axios.post("{{ route('card.sortupdate') }}", {
              sorted: sorted
            }).then(function (response) {
              saving.addClass('bg-green-300').animate({opacity: "0"}, 250, "swing", function () {
                $(this).removeClass('bg-green-300');
              });
            });
          });
        });
      </script>
      <script>
        $(".card-charts").each(function(i, obj) {
          var ctx = obj.getContext('2d');
          var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
                datasets: [{
                    label: 'Label',
                    data: [1724, 1532, 1378, 1466, 1539, 1634],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
          });
        });
      </script>
    </div>
  </main>
</x-layout>