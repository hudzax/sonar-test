<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cards;

class CardController extends Controller
{
    // save sorted cards
    public function sortUpdate(Request $request)
    {
        foreach ($request->input('sorted') as $index => $id) {
            $card = Cards::find($id);
            $card->order = $index;
            $card->save();
        }

        return 'OK';
    }
}
