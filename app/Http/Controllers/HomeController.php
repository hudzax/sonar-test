<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cards;
use App\Models\User;
use Auth;

class HomeController extends Controller
{
    public function index()
    {
        return view('home', [
            'cards' => Cards::where('user', Auth::user()->id)->orderBy('order')->get(),
            'active_users' => User::where('status_id', 1)->orderBy('name')->get()
        ]);
    }
}
